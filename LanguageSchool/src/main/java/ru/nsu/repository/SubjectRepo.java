package ru.nsu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.domain.Subject;

public interface SubjectRepo extends JpaRepository<Subject, Long> {
    Subject findBySubjectName(String subjectname);
}
