package ru.nsu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.domain.Branch;

public interface BranchRepo extends JpaRepository<Branch, Long> {
    Branch findByBranchName(String branchname);
}
