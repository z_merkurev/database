package ru.nsu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.domain.Manager;

public interface ManagerRepo extends JpaRepository<Manager, Long> {
    Manager findByManagerName(String name);

    Manager findByManagerNameAndManagerSurname(String name, String surname);
}
