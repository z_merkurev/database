package ru.nsu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.domain.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUserName(String username);
}
