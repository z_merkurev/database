package ru.nsu.utils;

public interface Constants {
    static final String managersTemplateName = "managers";
    static final String branchesTemplateName = "branches";
    static final String adminTemplateName = "isAdmin";
}
