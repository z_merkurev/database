package ru.nsu.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.domain.*;
import ru.nsu.repository.BranchRepo;
import ru.nsu.repository.ManagerRepo;
import ru.nsu.repository.SubjectRepo;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static ru.nsu.utils.Constants.*;

@Controller
@RequestMapping("/branches")
public class BranchController {
    private final BranchRepo branchRepo;
    private final ManagerRepo managerRepo;
    private final SubjectRepo subjectRepo;

    public BranchController(final BranchRepo branchRepo, final ManagerRepo managerRepo, final SubjectRepo subjectRepo) {
        this.branchRepo = branchRepo;
        this.managerRepo = managerRepo;
        this.subjectRepo = subjectRepo;
    }

    @GetMapping()
    public String branches(@AuthenticationPrincipal User user, Map<String, Object> model) {
        configureModel(user, model);
        return "branches";
    }

    @PostMapping()
    public String addBranch(@AuthenticationPrincipal User user, Branch branch, Map<String, Object> model) {
        Branch branchFromDb = branchRepo.findByBranchName(branch.getBranchName());
        if (null == branchFromDb && null != branch.getBranchName() && null != branch.getAddress() &&
                null != branch.getPhoneNumber()) {
            branchRepo.save(branch);
        }
        configureModel(user, model);
        return "branches";
    }

    @PostMapping("/del")
    public String deleteBranch(@AuthenticationPrincipal User user, @RequestParam("id") Branch branch,
                               Map<String, Object> model) {

        branchRepo.delete(branch);
        configureModel(user, model);
        return "redirect:/branches";
    }

    @GetMapping("/edit/{id}")
    public String editBranche(@PathVariable("id") Branch branch,
                              Map<String, Object> model) {
        model.put("currentManager", branch.getManager());
        List<Manager> managers = managerRepo.findAll();
        managers.remove(branch.getManager());
        model.put("subjects", branch.getSubject());
        //db -(service)-> model -(mapper)-> dto
        model.put("availableSubjects", subjectRepo.findAll());
        model.put(managersTemplateName, managers);
        model.put("branch", branch);
        return "/editbranch";
    }

    @GetMapping("/{id}")
    public String branchInfo(@PathVariable("id") Branch branch,
                             Map<String, Object> model) {
        model.put("currentManager", branch.getManager());
        model.put("subjects", branch.getSubject());
        model.put("branch", branch);
        return "/branchinfo";
    }

    @PostMapping("/edit/del-sub{id}")
    public String delBranchSubject(@PathVariable("id") Subject subject, Branch branch) {
        Branch tempBranch = branchRepo.findByBranchName(branch.getBranchName());
        tempBranch.getSubject().remove(subject);
        branchRepo.save(tempBranch);
        return "redirect:/branches/edit/" + branch.getId();
    }

    @PostMapping("/edit/add-sub{id}")
    public String addSubjectToBranch(@PathVariable("id") Branch branch, Subject subject) {
        Branch tempBranch = branchRepo.findByBranchName(branch.getBranchName());
        Subject tempSubject = subjectRepo.findBySubjectName(subject.getSubjectName());
        if (null == tempSubject) {
            return "redirect:/branches/edit/" + branch.getId();
        }
        tempBranch.getSubject().add(tempSubject);
        branchRepo.save(tempBranch);
        return "redirect:/branches/edit/" + branch.getId();
    }

    @PostMapping("/edit/{id}")
    public String updateBranche(Branch branch) {
        Branch tempBranch = branchRepo.findByBranchName(branch.getBranchName());
        branch.setSubject(tempBranch.getSubject());
        branchRepo.save(branch);
        return "redirect:/branches";
    }

    private void configureModel(User user, Map<String, Object> model) {
        model.put("name", user.getUserName());
        model.put(adminTemplateName, user.getAuthorities().contains(Role.ADMIN));
        List<Branch> branches = branchRepo.findAll();
        branches.sort(Comparator.comparing(Branch::getId));
        Iterable<Manager> managers = managerRepo.findAll();
        model.put(managersTemplateName, managers);
        model.put(branchesTemplateName, branches);
    }
}
