package ru.nsu.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import ru.nsu.domain.Role;
import ru.nsu.domain.User;

import java.util.Map;

import static ru.nsu.utils.Constants.adminTemplateName;

@Controller
public class HomeController {
    @GetMapping("/home")
    public String home(@AuthenticationPrincipal User user, Map<String, Object> model) {
        model.put("name", user.getUserName());
        model.put(adminTemplateName, user.getAuthorities().contains(Role.ADMIN));
        return "home";
    }
}
