package ru.nsu.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.nsu.domain.Manager;
import ru.nsu.domain.Role;
import ru.nsu.domain.User;
import ru.nsu.repository.ManagerRepo;

import java.util.Map;

import static ru.nsu.utils.Constants.adminTemplateName;
import static ru.nsu.utils.Constants.managersTemplateName;

@Controller
@RequestMapping("/managers")
public class ManagerController {
    private final ManagerRepo managerRepo;

    public ManagerController(final ManagerRepo managerRepo) {
        this.managerRepo = managerRepo;
    }

    @GetMapping()
    public String managers(@AuthenticationPrincipal User user, Map<String, Object> model) {
        model.put("name", user.getUserName());
        Iterable<Manager> managers = managerRepo.findAll();
        model.put("warning", "");
        model.put(managersTemplateName, managers);
        model.put(adminTemplateName, user.getAuthorities().contains(Role.ADMIN));
        return "managers";
    }

    @PostMapping()
    public String addManager(@AuthenticationPrincipal User user, Manager manager, Map<String, Object> model) {
        Manager managerFromDb = managerRepo.findByManagerNameAndManagerSurname(manager.getManagerName(),
                manager.getManagerSurname());
        if (null == managerFromDb && null != manager.getManagerName() && null != manager.getManagerSurname()) {
            managerRepo.save(manager);
            model.put("warning", "");
        } else {
            model.put("warning", "Уже существует или неправильные входные данные");
        }


        model.put("name", user.getUserName());
        Iterable<Manager> managers = managerRepo.findAll();
        model.put(managersTemplateName, managers);
        model.put(adminTemplateName, user.getAuthorities().contains(Role.ADMIN));
        return "managers";
    }

    @PostMapping("/del")
    public String deleteManager(@AuthenticationPrincipal User user, @RequestParam("id") Manager manager,
                                Map<String, Object> model) {

        managerRepo.delete(manager);

        model.put("name", user.getUserName());
        model.put("warning", "");

        Iterable<Manager> managers = managerRepo.findAll();
        model.put(managersTemplateName, managers);
        model.put(adminTemplateName, user.getAuthorities().contains(Role.ADMIN));
        return "redirect:/managers";
    }
}
