package ru.nsu.domain;

import javax.persistence.*;

@Entity
@Table(name = "managers")
public class Manager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String managerName;
    private String managerSurname;

    public void setId(final Long id) {
        this.id = id;
    }

    public void setId(final String id) {
        if ("null".equals(id)) {
            this.id = null;
        } else {
            this.id = Long.valueOf(id);
        }
    }

    public Long getId() {
        return id;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(final String managerName) {
        this.managerName = managerName;
    }

    public String getManagerSurname() {
        return managerSurname;
    }

    public void setManagerSurname(final String managerSurname) {
        this.managerSurname = managerSurname;
    }

    public String getFullName() {
        return managerName + " " + managerSurname;
    }
}
