package ru.nsu.domain;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    TEACHER, ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }
}
