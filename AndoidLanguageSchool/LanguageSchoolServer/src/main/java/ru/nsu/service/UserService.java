package ru.nsu.service;

import org.springframework.stereotype.Service;
import ru.nsu.repository.UserRepository;

@Service
public class UserService {

    private final UserRepository userRepo;

    public UserService(final UserRepository userRepo) {
        this.userRepo = userRepo;
    }

}
