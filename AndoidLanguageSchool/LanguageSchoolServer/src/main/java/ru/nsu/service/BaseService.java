package ru.nsu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.nsu.exceptions.NotFoundException;
import ru.nsu.repository.BaseRepository;

import java.util.List;


abstract class BaseService<V> {

    @Autowired
    protected BaseRepository repository;

    BaseService(BaseRepository r) {
        this.repository = r;
    }

    public void deleteById(Long id) {
     //   repository.deleteById(id);
    }

    public void updateObject(V obj){
       // repository.saveAndFlush(obj);
    }

    public List<V> getAllObjects() {
        return null;//repository.findAll();
    }

    public V getById(Long id) {
        return null;//repository.findById(id).orElseThrow(NotFoundException::new);
    }
}
