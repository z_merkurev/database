package ru.nsu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.domain.Manager;
import ru.nsu.repository.ManagerRepository;
import ru.nsu.utils.ManagerWrapper;

import java.util.List;

@Service
public class ManagerService {
    @Autowired
    private ManagerRepository repository;

    public boolean addManager(Manager manager) {
        Manager managerFromDb = repository.findByManagerNameAndManagerSurname(manager.getManagerName(), manager.getManagerSurname());
        if (null == managerFromDb && null != manager.getManagerName() && !manager.getManagerName().equals("") &&
            null != manager.getManagerSurname() && !manager.getManagerSurname().equals("")) {
            repository.saveAndFlush(manager);
            return true;
        }
        return false;
    }

    public boolean deleteById(Long id) {
        Manager m = repository.findById(id).orElse(null);
        if (null == m) {
            return false;
        }
        repository.deleteById(id);
        return true;
    }

    public boolean updateObject(Manager obj){
        Manager m = repository.findById(obj.getId()).orElse(null);
        if (null == m) {
            return false;
        }
        m.setManagerName(obj.getManagerName());
        m.setManagerSurname(obj.getManagerSurname());
        repository.saveAndFlush(m);
        return true;
    }

    public List<Manager> getAllObjects() {
        return repository.findAll();
    }

    public ManagerWrapper getManagerWrapperById(Long id) {
        Manager m = repository.findById(id).orElse(null);
        if (null == m) {
            return null;
        }
        return new ManagerWrapper(m.getId(), m.getManagerName(), m.getManagerSurname());
    }

    public Manager getManagerById(Long id) {
        return repository.findById(id).orElse(null);
    }
}
