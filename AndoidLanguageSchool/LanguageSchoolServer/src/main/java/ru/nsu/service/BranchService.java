package ru.nsu.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.domain.Branch;
import ru.nsu.domain.Course;
import ru.nsu.domain.Group;
import ru.nsu.exceptions.NotFoundException;
import ru.nsu.repository.BranchRepository;
import ru.nsu.utils.BranchWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class BranchService {

    @Autowired
    BranchRepository repository;


    public boolean addBranch(Branch branch) {
        Branch branchFromDb = repository.findByBranchName(branch.getBranchName());
        if (null == branchFromDb &&
                null != branch.getBranchName() && !branch.getBranchName().equals("") &&
                null != branch.getAddress() && !branch.getAddress().equals("") &&
                null != branch.getPhoneNumber() && !branch.getPhoneNumber().equals("") &&
                null != branch.getManager()) {
            repository.saveAndFlush(branch);
            return true;
        }
        return false;
    }
    public boolean deleteById(Long id) {
        Branch obj = repository.findById(id).orElse(null);
        if (null == obj) {
            return false;
        }
        repository.deleteById(id);
        return true;
    }

    public boolean updateObject(Branch obj){
        Branch b = repository.findById(obj.getId()).orElse(null);
        if (null == b) {
            return false;
        }
        b.setBranchName(obj.getBranchName());
        b.setAddress(obj.getAddress());
        b.setPhoneNumber(obj.getPhoneNumber());
        if (null != obj.getManager()) {
            b.setManager(obj.getManager());
        }
        if (null != obj.getCourses()) {
            b.setCourses(obj.getCourses());
        }
        repository.saveAndFlush(b);
        return true;
    }

    public List<Group> getGroups(Branch branch) {
        Set<Course> courses = branch.getCourses();
        List<Group> groups = new ArrayList<>();
        for (Course c : courses) {
            groups.addAll(c.getGroups());
        }
        return groups;
    }

    public List<BranchWrapper> getAllObjects() {
        List<Branch> branches = repository.findAll();
        return branches.stream().
                map(b -> new BranchWrapper(b.getId(), b.getBranchName(), b.getAddress(), b.getPhoneNumber(), b.getManager())).
                collect(Collectors.toList());
    }

    public Branch getById(Long id) {
        return repository.findById(id).orElse(null);
    }
}