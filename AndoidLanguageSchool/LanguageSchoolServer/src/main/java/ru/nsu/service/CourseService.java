package ru.nsu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.domain.Course;
import ru.nsu.exceptions.NotFoundException;
import ru.nsu.repository.CourseRepository;
import ru.nsu.utils.CourseWrapper;

import java.util.List;

@Service
public class CourseService {
    @Autowired
    private CourseRepository repository;

    public boolean addCourse(Course course) {
        Course courseFromDb = repository.findCourseByName(course.getName());
        if (null == courseFromDb && null != course.getName() && !course.getName().equals("") &&
            null != course.getLevel() && !course.getLevel().equals("") && null != course.getBranches()) {
            repository.saveAndFlush(course);
            return true;
        }
        return false;
    }
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public boolean updateObject(Course obj) {
        Course course = repository.findById(obj.getId()).orElse(null);
        if (null == course) {
            return false;
        }
        course.setName(obj.getName());
        course.setLevel(obj.getLevel());
        if (null != obj.getBranches()) {
            course.setBranches(obj.getBranches());
        }
        if (null != obj.getGroups()) {
            course.setGroups(obj.getGroups());
        }
        repository.saveAndFlush(course);
        return true;
    }

    public List<Course> getAllObjects() {
        return repository.findAll();
    }

    public Course getCourseById(Long id) {
        Course c = repository.findById(id).orElse(null);
        if (null == c)
            return null;
        return c;
    }

    public CourseWrapper getCourseWrapperById(Long id) {
        Course c = repository.findById(id).orElse(null);
        if (null == c)
            return null;
        return new CourseWrapper(c.getId(), c.getName(), c.getLevel(), c.getBranches(), c.getGroups());
    }
}
