package ru.nsu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.domain.Teacher;
import ru.nsu.repository.TeacherRepository;

import java.util.List;

@Service
public class TeacherService  {

    @Autowired
    private TeacherRepository repository;
    public boolean addTeacher(Teacher teacher) {
        Teacher teacherFromDb = repository.findTeacherBySurname(teacher.getSurname());
        if (null == teacherFromDb && null != teacher.getName() && !teacher.getName().equals("") &&
            null != teacher.getSurname() && !teacher.getSurname().equals("")) {
            repository.saveAndFlush(teacher);
            return true;
        }
        return false;
    }
    public boolean deleteById(Long id) {
        Teacher teacher = repository.findById(id).orElse(null);
        if (null == teacher) {
            return false;
        }
        repository.deleteById(id);
        return true;
    }

    public boolean updateObject(Teacher obj){
        Teacher teacher = repository.findById(obj.getId()).orElse(null);
        if (null == teacher) {
            return false;
        }
        teacher.setName(obj.getName());
        teacher.setSurname(obj.getSurname());
        if (null != obj.getGroups()) {
            teacher.setGroups(obj.getGroups());
        }
        repository.saveAndFlush(teacher);
        return true;
    }

    public List<Teacher> getAllObjects() {
        return repository.findAll();
    }

    public Teacher getById(Long id) {
        return repository.findById(id).orElse(null);
    }
}
