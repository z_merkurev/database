package ru.nsu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.domain.Group;
import ru.nsu.exceptions.NotFoundException;
import ru.nsu.repository.GroupRepository;

import java.util.List;

@Service
public class GroupService {

    @Autowired
    GroupRepository repository;

    public boolean addGroup(Group group) {
        Group groupFromDb = repository.findByNumber(group.getNumber());
        if (null == groupFromDb &&
                0 != group.getNumber() && null != group.getTeacher() &&
                null != group.getCourse()) {
            repository.saveAndFlush(group);
            return true;
        }
        return false;
    }

    public boolean deleteById(Long id) {
        Group g = repository.findById(id).orElse(null);
        if (null == g) {
            return false;
        }
        repository.deleteById(id);
        return true;
    }

    public boolean updateObject(Group obj){
        Group g = repository.findById(obj.getId()).orElse(null);
        if (null == g) {
            return false;
        }
        g.setNumber(obj.getNumber());
        if (null != obj.getTeacher()) {
            g.setTeacher(obj.getTeacher());
        }
        if (null != obj.getCourse()) {
            g.setCourse(obj.getCourse());
        }
        repository.saveAndFlush(g);
        return true;
    }

    public List<Group> getAllObjects() {
        return repository.findAll();
    }

    public Group getById(Long id) {
        return repository.findById(id).orElse(null);
    }
}
