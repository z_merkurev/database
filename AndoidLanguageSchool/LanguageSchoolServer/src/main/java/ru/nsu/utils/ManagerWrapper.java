package ru.nsu.utils;

import ru.nsu.domain.Branch;

import java.util.Set;

public class ManagerWrapper {
    private Long id;
    private String name;
    private String surname;
  //  private Set<BranchWrapper> branchSet;

    public ManagerWrapper(Long id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
      //  this.branchSet = set;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    //public Set<BranchWrapper> getBranchSet() {
      //  return branchSet;
    //}

    //public void setBranchSet(Set<BranchWrapper> branchSet) {
      //  this.branchSet = branchSet;
    //}

}
