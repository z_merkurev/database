package ru.nsu.utils;

import ru.nsu.domain.Manager;

public class BranchWrapper {
    private Long id;
    private String branchName;
    private String address;
    private String phoneNumber;
    private Manager manager;

    public BranchWrapper(Long id, String branchName, String address, String phoneNumber, Manager manager) {
        this.id = id;
        this.branchName = branchName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.manager = manager;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }
    
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
