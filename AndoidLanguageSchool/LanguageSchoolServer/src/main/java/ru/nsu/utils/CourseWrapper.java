package ru.nsu.utils;

import ru.nsu.domain.Branch;
import ru.nsu.domain.Group;

import java.util.Set;

public class CourseWrapper {
    private Long id;
    private String name;
    private String level;
    private Set<Branch> branchSet;
    private Set<Group> groupSet;

    public CourseWrapper(Long id, String name, String level, Set<Branch> branchSet, Set<Group> groupSet) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.branchSet = branchSet;
        this.groupSet = groupSet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Set<Branch> getBranchSet() {
        return branchSet;
    }

    public void setBranchSet(Set<Branch> branchSet) {
        this.branchSet = branchSet;
    }

    public Set<Group> getGroupSet() {
        return groupSet;
    }

    public void setGroupSet(Set<Group> groupSet) {
        this.groupSet = groupSet;
    }
}
