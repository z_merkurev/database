package ru.nsu.domain;

public enum Role {
    TEACHER, ADMIN;
}
