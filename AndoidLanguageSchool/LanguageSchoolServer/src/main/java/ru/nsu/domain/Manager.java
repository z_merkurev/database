package ru.nsu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "manager")
public class Manager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "manager_id")
    private Long id;

    private String managerName;
    private String managerSurname;

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "manager")
//    @JsonIgnore
//    private Set<Branch> branches;

    public void setId(final Long id) {
        this.id = id;
    }

    public void setId(final String id) {
        if ("null".equals(id)) {
            this.id = null;
        } else {
            this.id = Long.valueOf(id);
        }
    }

    public Long getId() {
        return id;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(final String managerName) {
        this.managerName = managerName;
    }

    public String getManagerSurname() {
        return managerSurname;
    }

    public void setManagerSurname(final String managerSurname) {
        this.managerSurname = managerSurname;
    }

//    public Set<Branch> getBranches() {
//        return branches;
//    }
//
//    public void setBranches(Set<Branch> branches) {
//        this.branches = branches;
//    }
}
