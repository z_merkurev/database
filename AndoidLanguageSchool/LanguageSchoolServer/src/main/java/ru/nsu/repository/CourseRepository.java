package ru.nsu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.domain.Course;

public interface CourseRepository extends JpaRepository<Course, Long> {
    Course findCourseByName(String name);
}
