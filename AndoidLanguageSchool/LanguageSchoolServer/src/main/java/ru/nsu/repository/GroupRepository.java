package ru.nsu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.domain.Group;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findByNumber(int number);
}
