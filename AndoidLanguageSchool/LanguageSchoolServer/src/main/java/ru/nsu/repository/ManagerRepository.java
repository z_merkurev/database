package ru.nsu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.domain.Manager;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
    Manager findByManagerNameAndManagerSurname(String name, String surname);
}
