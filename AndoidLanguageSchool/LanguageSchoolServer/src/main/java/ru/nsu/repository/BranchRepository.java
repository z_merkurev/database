package ru.nsu.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.domain.Branch;


public interface BranchRepository extends JpaRepository<Branch, Long> {
    Branch findByBranchName(String name);
}
