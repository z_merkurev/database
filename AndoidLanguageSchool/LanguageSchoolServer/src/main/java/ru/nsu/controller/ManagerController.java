package ru.nsu.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.domain.Manager;
import ru.nsu.service.ManagerService;
import ru.nsu.utils.ManagerWrapper;

import java.util.List;

@RestController
public class ManagerController {
    private final ManagerService service;

    public ManagerController(final ManagerService service) {
        this.service = service;
    }

    @GetMapping("/managers")
    public ResponseEntity<List<Manager>> managers() {
        List<Manager> managers = service.getAllObjects();
        if (null == managers) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(managers, HttpStatus.OK);
        //return service.getAllBranches();
    }

    @PostMapping("/managers")
    public ResponseEntity<Manager> addManager(@RequestBody Manager manager) {
        if (null == manager) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.addManager(manager)) {
            return new ResponseEntity<>(manager, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/managers/{id}")
    public ResponseEntity<Manager> deleteManager(@PathVariable Long id) {
        Manager manager = service.getManagerById(id);
        if (null == manager) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.deleteById(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        //return service.deleteById(id);
    }

    @GetMapping("/managers/{id}")
    public ResponseEntity<ManagerWrapper> managerOne(@PathVariable("id") Long id) {
        if (null == id) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        ManagerWrapper manager = service.getManagerWrapperById(id);
        if (null == manager) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(manager, HttpStatus.OK);
    }

    @PutMapping("/managers/{id}")
    public ResponseEntity<Manager> updateManager(@RequestBody Manager manager) {
        if (null == manager) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.updateObject(manager)) {
            return new ResponseEntity<>(manager, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
