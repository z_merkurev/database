package ru.nsu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.domain.Course;
import ru.nsu.domain.Group;
import ru.nsu.service.CourseService;
import ru.nsu.utils.CourseWrapper;

import java.util.List;

@RestController
public class CourseController {
    @Autowired
    private final CourseService service;

    public CourseController(CourseService service) {
        this.service = service;
    }

    @GetMapping("/courses")
    public ResponseEntity<List<Course>> getAllCourses() {
        List<Course> Courses = service.getAllObjects();
        if (null == Courses) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(Courses, HttpStatus.OK);
    }

    @PostMapping("/courses")
    public ResponseEntity<Course> addCourse(@RequestBody Course Course) {
        if (null == Course) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.addCourse(Course)) {
            return new ResponseEntity<>(Course, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/courses/{id}")
    public ResponseEntity<CourseWrapper> deleteCourse(@PathVariable Long id) {
        Course course = service.getCourseById(id);
        if (null == course) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        service.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("courses/{id}")
    public ResponseEntity<Course> updateCourse(@RequestBody Course Course) {
        if (null == Course) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.updateObject(Course)) {
            return new ResponseEntity<>(Course, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/courses/{id}")
    public ResponseEntity<CourseWrapper> CourseOne(@PathVariable("id") Long id) {
        CourseWrapper Course = service.getCourseWrapperById(id);
        if (null == Course) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(Course, HttpStatus.OK);
    }

    @PostMapping("/courses/{id}/group")
    public ResponseEntity<Course> addGroupById(@PathVariable("id") Long id, @RequestBody Group group) {
        if (null == group) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Course course = service.getCourseById(id);
        if (null == course) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        course.addGroup(group);
        service.updateObject(course);
        return new ResponseEntity<>(course, HttpStatus.OK);
    }
}