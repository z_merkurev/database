package ru.nsu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/startpage")
public class StartPageController {

    @GetMapping({"/startpage"})
    public String startPage() {
        return "startPage";
    }
}
