package ru.nsu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HomeController {
    @GetMapping("/home")
    public String home(Map<String, Object> model) {
//        model.put("name", user.getUserName());
//        model.put(adminTemplateName, user.getAuthorities().contains(Role.ADMIN));
        return "home";
    }
}
