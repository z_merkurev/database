package ru.nsu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.domain.Branch;
import ru.nsu.domain.Course;
import ru.nsu.domain.Group;
import ru.nsu.domain.Manager;
import ru.nsu.service.BranchService;
import ru.nsu.service.ManagerService;
import ru.nsu.utils.BranchWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@RestController
public class BranchController {
    @Autowired
    private final BranchService service;

    public BranchController(final BranchService service) {
        this.service = service;
    }

    @GetMapping("/branches")
    public ResponseEntity<List<BranchWrapper>> getBranches() {
        List<BranchWrapper> branches = service.getAllObjects();
        if (null == branches) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(branches, HttpStatus.OK);
        //return service.getAllBranches();
    }

    @PostMapping("/branches")
    public ResponseEntity<Branch> addBranch(@RequestBody Branch branch) {
        if (null == branch) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.addBranch(branch)) {
            return new ResponseEntity<>(branch, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/branches/{id}")
    public ResponseEntity<Branch> deleteBranchById(@PathVariable Long id) {
        Branch branch = service.getById(id);
        if (null == branch) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.deleteById(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        //return service.deleteById(id);
    }

    @GetMapping("/branches/{id}")
    public ResponseEntity<Set<Course>> getCourseByBranchId(@PathVariable("id") Long id) {
        if (null == id) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Branch branch = service.getById(id);
        if (null == branch) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(branch.getCourses(), HttpStatus.OK);
    }

    @GetMapping("/branches/{id}/groups")
    public ResponseEntity<List<Group>> getGroupsByBranchId(@PathVariable("id") Long id) {
        if (null == id) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Branch branch = service.getById(id);
        if (null == branch) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(service.getGroups(branch), HttpStatus.OK);
    }

    @PutMapping("/branches/{id}")
    public ResponseEntity<Branch> updateBranch(@RequestBody Branch branch) {
        if (null == branch) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.updateObject(branch)) {
            return new ResponseEntity<>(branch, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/branches/{id}/course")
    public ResponseEntity<Branch> addCourseById(@PathVariable("id") Long id, @RequestBody Course course) {
        if (null == course) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Branch branch = service.getById(id);
        if (null == branch) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        branch.addCourse(course);
        service.updateObject(branch);
        return new ResponseEntity<>(branch, HttpStatus.OK);
    }
}