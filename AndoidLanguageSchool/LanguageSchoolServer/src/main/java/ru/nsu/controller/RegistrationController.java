package ru.nsu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.domain.Role;
import ru.nsu.domain.User;
import ru.nsu.repository.UserRepository;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/registration")
public class RegistrationController {
    private UserRepository userRepo;

    @GetMapping
    public String registration(Map<String, Object> model) {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, Map<String, Object> model) {
        User userFromDb = userRepo.findByUserName(user.getUserName());
        if (null != userFromDb) {
            return "registration";
        }
        Set<Role> roles = new HashSet<>();
        roles.add(Role.TEACHER);
        user.setRoles(roles);
        user.setActive(true);
        userRepo.save(user);
        return "redirect:/login";
    }
}
