package ru.nsu.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.domain.Course;
import ru.nsu.domain.Group;
import ru.nsu.domain.Teacher;
import ru.nsu.service.GroupService;

import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping
public class GroupController {
    @Autowired
    private final GroupService service;

    public GroupController(GroupService service) {
        this.service = service;
    }

    @GetMapping("/groups")
    public ResponseEntity<List<Group>> getAllGroups() {
        List<Group> groups = service.getAllObjects();
        if (null == groups) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(groups, HttpStatus.OK);
    }

    @PostMapping("/groups")
    public ResponseEntity<Group> addGroup(@RequestBody Group group) {
        if (service.addGroup(group)) {
            return new ResponseEntity<>(group, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/groups/{id}")
    public ResponseEntity<Group> deleteGroup(@PathVariable Long id) {
        if (service.deleteById(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("groups/{id)")
    public ResponseEntity<Group> updateGroup(@RequestBody Group group) {
        if (null == group) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (service.updateObject(group)) {
            return new ResponseEntity<>(group, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/groups/{id}")
    public ResponseEntity<Group> groupOne(@PathVariable("id") Long id) {
        Group group = service.getById(id);
        if (null == group) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(group, HttpStatus.OK);
    }
}
